package pcd.lab02.matmul;

public class TestMul {

	public static void main(String[] args) throws Exception {
		int n = 1000; // 4; //100; // Integer.parseInt(args[0]);
		int k = 1000; // 4; // 100; // Integer.parseInt(args[1]);
		int m = 1000; // 4; // 100; // Integer.parseInt(args[2]);
		
		boolean debugging = false;

		System.out.println("Testing A["+n+","+k+"]*B["+k+","+m+"]");

		System.out.println("Initialising...");

		Mat matA = new Mat(n,m);
		matA.initRandom(10);

		if (debugging) {
			System.out.println("A:");
			matA.print();
		}
		
		Mat matB = new Mat(m,k);
		matB.initRandom(10);
		
		if (debugging){
			System.out.println("B:");
			matB.print();
		}
		
		System.out.println("Initialising done.");
		System.out.println("Computing matmul...");

		StopWatch cron = new StopWatch();
		cron.start();
		Mat matC = Mat.mul(matA, matB);
		cron.stop();
		
		System.out.println("Computing matmul done.");

		if (debugging){
			System.out.println("C:");
			matC.print();
		}
		
		System.out.println("Time elapsed: "+cron.getTime()+" ms.");
		
	}

}
